import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MentionHashtagsComponent } from './mention-hashtags.component';
import { MentionDirective } from './mention-hashtag.directive';
import { MentionListComponent } from './mention-list.component';



@NgModule({
  declarations: [MentionHashtagsComponent,
    MentionDirective,
    MentionListComponent],
  imports: [BrowserModule
  ],
  exports: [MentionHashtagsComponent, 
      MentionDirective],
  entryComponents: [
    MentionListComponent
  ]
})
export class MentionHashtagsModule { }
