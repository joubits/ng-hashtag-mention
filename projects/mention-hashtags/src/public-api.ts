/*
 * Public API Surface of mention-hashtags
 */

export * from './lib/mention-hashtags.service';
export * from './lib/mention-hashtags.component';
export * from './lib/mention-hashtags.module';
