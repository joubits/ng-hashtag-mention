import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MentionHashtagsModule } from 'mention-hashtags';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MentionHashtagsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
