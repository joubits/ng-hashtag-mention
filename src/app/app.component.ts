import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  mentionConfig = {
    mentions: [
        {
            items: [ "Jou", "Adam", "Andres", "Jay", "Latha" ],
            triggerChar: '@'
        },
        {
            items: [ "rig", "nabors", "well", "navigator" ],
            triggerChar: '#'
        }
    ]
  }
}
